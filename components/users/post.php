<?php 
	// INCLUDE CLASS
	require 'classes/User.php';

	$objAlbum = new User();
	
	$params 	= json_decode(file_get_contents('php://input'), true);
	$name 		= $match['name'];

	if($params){
		// GET USER BY ID
		if($name == 'user-new'){
			$response = $objAlbum->insertNewUser($params);
			echo json_encode($response);
		}
		if($name == 'login'){
			$response = $objAlbum->login($params);
			echo json_encode($response);
		}
		if($name == 'user-update'){
			$response = $objAlbum->updateUser($params);
			echo json_encode($response);
		}
	} else {
		echo json_encode( array("response" => 'err') );
	}
?>