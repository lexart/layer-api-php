<?php 
	require 'classes/Album.php';

	$objAlbum = new Album();

	$params 	= $match['params'];
	$name 		= $match['name'];
		
	// GET STUDENT BY ID
	if($name == 'album-by-user'){
		$id 		= $params["token"];
		$response 	= $objAlbum->getAlbumesByUser($id);
		echo json_encode($response);
	}
	if($name == 'album-by-id-user'){
		$token 		= $params["token"];
		$id 		= $params["id"];
		$response 	= $objAlbum->getAlbumesByUserAndId($token, $id);
		echo json_encode($response);
	}
?>